package main

import (
	"flag"
	"fmt"
	"os"
	"path"

	"gitlab.com/gl-infra/gitlab-patcher/config"
	"gitlab.com/gl-infra/gitlab-patcher/knife"
	"gitlab.com/gl-infra/gitlab-patcher/manifest"
	"gitlab.com/gl-infra/gitlab-patcher/patcher"
)

var usage = `Usage:

%s [options] <version> <environment>

`

// Version is the program version
const Version = "0.1.4"

func main() {
	wd, err := os.Getwd()
	if err != nil {
		exitWithError(fmt.Errorf("could not get the current working dir: %s", err))
	}

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage, os.Args[0])
		flag.PrintDefaults()
	}
	workdir := flag.String("workdir", wd, "working dir, folder in which to look for manifest and patches, by default is the current folder")
	manfilename := flag.String("manifest", "MANIFEST.yml", "configuration file to use, defaults to MANIFEST.yaml")
	chefrepo := flag.String("chef-repo", os.Getenv("GITLAB_CHEF_REPO_DIR"), "chef repo dir, defaults to the environment var GITLAB_CHEF_REPO_DIR")
	mode := flag.String("mode", config.ModeDryRun, fmt.Sprintf("execution mode, can be %s (by default) %s or %s",
		config.ModeDryRun, config.ModePatch, config.ModeRollback))
	showVersion := flag.Bool("version", false, "prints the version and exits")

	flag.Parse()

	args := flag.Args()

	if *showVersion {
		exitWithVersion()
	}

	go func() {
		exitWithError(recover())
	}()

	version, environment, err := checkArgs(*workdir, *chefrepo, *mode, args...)
	exitWithUsage(err)

	m, err := readManifest(path.Join(*workdir, *manfilename))
	exitWithError(err)

	if err := m.Validate(version, environment); err != nil {
		exitWithError(err)
	}

	p := patcher.New(config.New(
		m,
		version,
		environment,
		*workdir,
		*mode))

	if err = p.Patch(os.Stdout, knife.New(*chefrepo)); err != nil {
		exitWithError(err)
	}
}

func exitWithVersion() {
	fmt.Printf("Version: %s\n\n", Version)
	flag.Usage()
	os.Exit(0)
}

func exitWithUsage(err error) {
	if err != nil {
		fmt.Printf("%s\n\n", err)
		flag.Usage()
		os.Exit(1)
	}
}

func exitWithError(err interface{}) {
	if err != nil {
		fmt.Printf("Error: %s\n\n", err)
		os.Exit(1)
	}
}

func checkDir(message, d string) error {
	if d == "" {
		return fmt.Errorf("%s is empty", message)
	}
	f, err := os.Stat(d)
	if err != nil {
		return fmt.Errorf("%s, %s", message, err)
	}
	if !f.IsDir() {
		return fmt.Errorf("%s %s is not a directory", message, d)
	}
	return nil
}

func checkArgs(workdir, chefrepo, mode string, args ...string) (version string, environment string, err error) {
	if len(args) != 2 {
		err = fmt.Errorf("wrong number of arguments, expected 2")
		return
	}

	validModes := map[string]bool{config.ModeDryRun: true, config.ModePatch: true, config.ModeRollback: true}
	if _, ok := validModes[mode]; !ok {
		err = fmt.Errorf("invalid mode %s", mode)
	}

	if err = checkDir("workdir", workdir); err != nil {
		return
	}
	if err = checkDir("chef repo", chefrepo); err != nil {
		return
	}

	version = args[0]
	environment = args[1]

	return
}

func readManifest(filename string) (manifest.Manifest, error) {
	r, err := os.Open(filename)
	if err != nil {
		return manifest.Manifest{}, err
	}
	defer r.Close()

	return manifest.New(r)
}
