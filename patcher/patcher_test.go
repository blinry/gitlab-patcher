package patcher_test

import (
	"bytes"
	"io/ioutil"
	"os"
	"path"
	"testing"

	"gitlab.com/gl-infra/gitlab-patcher/config"
	"gitlab.com/gl-infra/gitlab-patcher/knife"
	"gitlab.com/gl-infra/gitlab-patcher/manifest"
	"gitlab.com/gl-infra/gitlab-patcher/patcher"
)

var m = manifest.Manifest{
	Patches: map[string]manifest.Patch{
		"patch.diff": manifest.Patch{
			Checksum: "437b930db84b8079c2dd804a71936b5f",
			IssueURL: "https://gitlab.com/issues",
			BaseDir:  "/tmp",
			Level:    1,
		},
	},
	Versions: map[string]manifest.Version{
		"0.9": manifest.Version{
			Environments: map[string][]string{
				"prod": []string{"lbs", "web-fe"},
			},
			Patches: []string{
				"patch.diff",
			},
		},
	},
}

func TestPatching(t *testing.T) {
	wd, err := ioutil.TempDir("", "")
	check(err, "failed to create a temp dir", t)
	defer os.RemoveAll(wd)

	ioutil.WriteFile(path.Join(wd, "patch.diff"), []byte("something"), 0644)

	tt := []struct {
		name     string
		mode     string
		expected string
	}{
		{
			name: "DryRun",
			mode: config.ModeDryRun,
			expected: `GitLab Patcher
Mode: dryrun
Patches: patch.diff
Roles: lbs, web-fe
------------------------------------
Resolving hostnames...
Resolved hosts: 
Uploading patches...
ssh roles:lbs OR roles:web-fe echo 'c29tZXRoaW5n' | base64 -w 0 -di > patch.diff
Writing patches checksums file...
ssh roles:lbs OR roles:web-fe echo 'NDM3YjkzMGRiODRiODA3OWMyZGQ4MDRhNzE5MzZiNWYgIHBhdGNoLmRpZmY=' | base64 -w 0 -di > patches.checksum
Verifying patches checksum...
ssh roles:lbs OR roles:web-fe md5sum -c patches.checksum
Applying patches in dryrun mode...
ssh roles:lbs OR roles:web-fe (cd /tmp && sudo patch -f -p1 --dry-run) < patch.diff
Done!
`},
		{
			name: "Patch",
			mode: config.ModePatch,
			expected: `GitLab Patcher
Mode: patch
Patches: patch.diff
Roles: lbs, web-fe
------------------------------------
Resolving hostnames...
Resolved hosts: 
Uploading patches...
ssh roles:lbs OR roles:web-fe echo 'c29tZXRoaW5n' | base64 -w 0 -di > patch.diff
Writing patches checksums file...
ssh roles:lbs OR roles:web-fe echo 'NDM3YjkzMGRiODRiODA3OWMyZGQ4MDRhNzE5MzZiNWYgIHBhdGNoLmRpZmY=' | base64 -w 0 -di > patches.checksum
Verifying patches checksum...
ssh roles:lbs OR roles:web-fe md5sum -c patches.checksum
Applying patches in patch mode...
ssh roles:lbs OR roles:web-fe (cd /tmp && sudo patch -f -p1) < patch.diff
Sending HUP to all unicorns to reload the code with a concurrency of 1 and 30 seconds sleep...
ssh -C 1 roles:lbs OR roles:web-fe echo 'reloading unicorn'; sudo gitlab-ctl hup unicorn; echo 'done reloading, sleeping'; sleep 30
Done!
`},
		{
			name: "Rollback",
			mode: config.ModeRollback,
			expected: `GitLab Patcher
Mode: rollback
Patches: patch.diff
Roles: lbs, web-fe
------------------------------------
Resolving hostnames...
Resolved hosts: 
Uploading patches...
ssh roles:lbs OR roles:web-fe echo 'c29tZXRoaW5n' | base64 -w 0 -di > patch.diff
Writing patches checksums file...
ssh roles:lbs OR roles:web-fe echo 'NDM3YjkzMGRiODRiODA3OWMyZGQ4MDRhNzE5MzZiNWYgIHBhdGNoLmRpZmY=' | base64 -w 0 -di > patches.checksum
Verifying patches checksum...
ssh roles:lbs OR roles:web-fe md5sum -c patches.checksum
Applying patches in rollback mode...
ssh roles:lbs OR roles:web-fe (cd /tmp && sudo patch -f -p1 -R) < patch.diff
Sending HUP to all unicorns to reload the code with a concurrency of 1 and 30 seconds sleep...
ssh -C 1 roles:lbs OR roles:web-fe echo 'reloading unicorn'; sudo gitlab-ctl hup unicorn; echo 'done reloading, sleeping'; sleep 30
Done!
`},
	}

	k := knife.Knife{
		Cmd:     []string{"echo"},
		ChefDir: wd,
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			p := patcher.New(config.New(m, "0.9", "prod", wd, tc.mode))

			buf := bytes.NewBuffer(nil)
			err = p.Patch(buf, k)

			check(err, "failed to patch", t)

			if buf.String() != tc.expected {
				t.Fatalf("Wrong output, got %s; expected %s", buf.String(), tc.expected)
			}
		})
	}
}

func check(err error, msg string, t *testing.T) {
	if err != nil {
		t.Fatal(msg, err)
	}
}
