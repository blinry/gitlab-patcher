<img src="https://gitlab.com/gl-infra/gitlab-patcher/raw/master/assets/gitlab-patcher-logo.png" alt="Tanuki with band-aids" align="right" title="CC by-nc-sa 4.0, Sebastian Morr" width="120">

[![coverage report](https://gitlab.com/gl-infra/gitlab-patcher/badges/master/coverage.svg)](https://gitlab.com/gl-infra/gitlab-patcher/commits/master)

[![pipeline status](https://gitlab.com/gl-infra/gitlab-patcher/badges/master/pipeline.svg)](https://gitlab.com/gl-infra/gitlab-patcher/commits/master)


# GitLab Patcher

This command line tool applies patches to the different GitLab fleets by using
manifest files describing how and which patches need to be applied for a given
version and environment.

It depends on the running user having knife properly setup and actuall access
to the fleet.

## Installing

Just use `go get gitlab.com/gl-infra/gitlab-patcher`

If it puts up a fight for any reason, you can install manually by running:

```
go get -u github.com/golang/dep/cmd/dep
git clone https://gitlab.com/gl-infra/gitlab-patcher.git
cd gitlab-patcher
dep ensure
go install
```

## Usage

```
./gitlab-patcher [options] <version> <environment>
./gitlab-patcher -mode patch [options] <version> <environment>
./gitlab-patcher -mode rollback [options] <version> <environment>
```

This will search for the Manifest file in the current working directory
(unless -workdir is issued) and will apply the patches as described in the
version and environment.

- Version: the version to load from the manifest.
- Environment: which environment to apply the patches to.

### Options

* `-chef-repo string`
  chef repo dir, defaults to the environment var GITLAB_CHEF_REPO_DIR

* `-manifest string`
  configuration file to use (default "MANIFEST.yml")

* `-mode string`
  execution mode, can be dryrun, patch or rollback (default "dryrun")

* `-workdir string`
  working dir, folder in which to look for manifest and patches, by default is
  the current folder


## Manifest sample

```yaml
patches:
  a-summary.diff:
    md5_checksum: abc
    issue_url: http://...
    level: 1
    base_dir: /var/opt/gitlab
  b-summary.diff:
    md5_checksum: dfc
    issue_url: http://...
    level: 2
    base_dir: /var/opt/gitlab
versions:
  10.0.0-rc2-ee:
    environments:
      canary:
        - canary-fe-base
        - canary-be-base
      prod:
        - gitlab-fe-base
        - gitlab-be-base
      staging:
        - staging-base
    patches:
      - a-summary.diff
      - b-summary.diff
```

### Patch configuration

* `md5_checksum`: the md5 of the patch file, it will be used to check that
  the file was transferred correctly
* `level`: the level of the patch, it translates to `-pN` in the `patch`
  command
* `base_dir`: the path in which the patch has to be applied

### Version configuration

* `environments`: a map of the different environments and the roles they
  apply to.
* `patches`: the list of patches to apply in this version.

## Execution Sample

With this sample manifest, the way to apply the patches to staging with the
current working dir located in the folder where the manifest and the patches
file are located looks like this:

```
./gitlab-patcher -mode patch 10.0.0-rc2-ee staging
```

# Post Deployment Patches

Official patches that are applied to GitLab.com can be tracked in
https://dev.gitlab.org/gitlab/post-deployment-patches

# Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)
