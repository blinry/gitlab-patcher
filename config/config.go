package config

import (
	"gitlab.com/gl-infra/gitlab-patcher/manifest"
)

// Patching modes
const (
	ModeDryRun   = "dryrun"
	ModePatch    = "patch"
	ModeRollback = "rollback"
)

// Config is all the configuration that a patcher needs to run
type Config struct {
	manifest.Manifest
	WorkingDir  string
	Mode        string
	version     string
	environment string
}

// New creates a configuration with the provided values
func New(manifest manifest.Manifest, version, environment, workdir, mode string) Config {
	return Config{
		Manifest:    manifest,
		WorkingDir:  workdir,
		Mode:        mode,
		version:     version,
		environment: environment,
	}
}

// GetVersion returns the version picked for this patching
func (c Config) GetVersion() manifest.Version {
	return c.Manifest.Versions[c.version]
}

// GetRolesToApply returns the chef roles query that matches all the roles
func (c Config) GetRolesToApply() []string {
	return c.GetVersion().Environments[c.environment]
}

// GetPatchesToApply returns the list of patches that are defined in the manifest of which patches to apply
func (c Config) GetPatchesToApply() []string {
	if !c.hasPatches() {
		return nil
	}

	return c.GetVersion().Patches
}

func (c Config) hasRoles() bool {
	return len(c.GetVersion().Environments[c.environment]) != 0
}

func (c Config) hasPatches() bool {
	return c.hasRoles() && len(c.GetVersion().Patches) != 0
}
