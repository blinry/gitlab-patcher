package manifest_test

import (
	"fmt"
	"io"
	"os"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gl-infra/gitlab-patcher/manifest"
)

func TestLoadingManifest(t *testing.T) {
	tt := []struct {
		name     string
		conf     func() (io.ReadCloser, error)
		manifest manifest.Manifest
	}{
		{
			name: "valid config",
			conf: func() (io.ReadCloser, error) { return os.Open("manifest-sample.yml") },
			manifest: manifest.Manifest{
				Patches: map[string]manifest.Patch{
					"a-summary.diff": manifest.Patch{Checksum: "abc", IssueURL: "http://dev.gitlab.org/security", Level: 0, BaseDir: "/var/opt/gitlab/app"},
					"b-summary.diff": manifest.Patch{Checksum: "dfc", IssueURL: "http://gitlab.com/issues", Level: 1, BaseDir: "/var/opt/gitlab"},
				},
				Versions: map[string]manifest.Version{
					"10.0.0-rc2-ee": manifest.Version{Environments: map[string][]string{
						"canary":  []string{"canary-fe-base", "canary-be-base"},
						"prod":    []string{"gitlab-fe-base", "gitlab-be-base"},
						"staging": []string{"staging-base"},
					},
						Patches: []string{
							"a-summary.diff",
							"b-summary.diff",
						},
					},
				},
			},
		},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			r, err := tc.conf()
			if err != nil {
				t.Fatalf("could not open config-sample.yml %s", err)
			}
			defer r.Close()

			actual, err := manifest.New(r)
			if err != nil {
				t.Fatalf("could not create a manifest %s", err)
			}

			assertEquals(t, tc.manifest, actual)

			if err = actual.Validate("10.0.0-rc2-ee", "canary"); err != nil {
				t.Fatalf("manifest is not valid %s", err)
			}
		})
	}
}

type badReader struct {
}

func (badReader) Read([]byte) (int, error) {
	return 0, fmt.Errorf("failed to read")
}

func TestBadReader(t *testing.T) {
	_, err := manifest.New(badReader{})
	if fmt.Sprint(err) != "could not read configuration: failed to read" {
		t.Fatalf("wrong error message, got %s", err)
	}
}

func TestInvalidManifest(t *testing.T) {
	_, err := manifest.New(strings.NewReader("invalid"))
	expected := `could not parse the configuration yaml: unmarshal errors:
  line 1: cannot unmarshal !!str ` + "`invalid` into manifest.Manifest"
	if fmt.Sprint(err) != expected {
		t.Fatalf("wrong error message, expected\n%s\ngot\n%s\n", expected, err)
	}
}

func TestManifestErrors(t *testing.T) {
	tt := []struct {
		name          string
		version       string
		environment   string
		manifest      manifest.Manifest
		expectedError string
	}{
		{"no version", "1", "prd", manifest.Manifest{}, "could not find version 1 in manifest"},
		{"no md5", "1", "prd", manifest.Manifest{
			Patches: map[string]manifest.Patch{
				"p": manifest.Patch{},
			},
		}, "patch p does not contain an md5 checksum"},
		{"no base dir", "1", "prd", manifest.Manifest{
			Patches: map[string]manifest.Patch{
				"p": manifest.Patch{
					Checksum: "a1",
				},
			},
		}, "patch p does not contain a base dir"},
		{"no env", "1", "prd", manifest.Manifest{
			Versions: map[string]manifest.Version{
				"1": manifest.Version{
					Patches: []string{},
				},
			},
		}, "could not find environment prd for version 1 in manifest"},
		{"no roles", "1", "prd", manifest.Manifest{
			Versions: map[string]manifest.Version{
				"1": manifest.Version{
					Patches: []string{},
					Environments: map[string][]string{
						"prd": []string{},
					},
				},
			},
		}, "there are no roles configured for environment prd on version 1"},
		{"no patches", "1", "prd", manifest.Manifest{
			Versions: map[string]manifest.Version{
				"1": manifest.Version{
					Patches: []string{},
					Environments: map[string][]string{
						"prd": []string{"something"},
					},
				},
			},
		}, "there are no patches configured for environment prd on version 1"},
		{"no p1 patch", "1", "prd", manifest.Manifest{
			Versions: map[string]manifest.Version{
				"1": manifest.Version{
					Patches: []string{"p1"},
					Environments: map[string][]string{
						"prd": []string{"something"},
					},
				},
			},
		}, "could not find patch p1 referenced in version prd and environment 1"},
	}
	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			err := tc.manifest.Validate(tc.version, tc.environment)
			if err == nil {
				t.Fatalf("Test should have failed with %s but didn't", tc.expectedError)
			}
			if fmt.Sprint(err) != tc.expectedError {
				t.Fatalf("Expected error %s; got %s", tc.expectedError, err)
			}
		})
	}
}

func assertEquals(t *testing.T, actual, expected interface{}) {
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf("expected %v; got %v", expected, actual)
	}
}
