package manifest

import (
	"fmt"
	"io"
	"io/ioutil"
	"strings"

	"gopkg.in/yaml.v2"
)

// Patch represents a single patch metadata to be applied
type Patch struct {
	Checksum string `yaml:"md5_checksum"`
	IssueURL string `yaml:"issue_url"`
	Level    int    `yaml:"level"`
	BaseDir  string `yaml:"base_dir"`
}

// Version provides a map of environments and patches to apply.
// The environments in turn contain the roles that will be resolved
// through Knife in runtime
type Version struct {
	Environments map[string][]string `yaml:"environments"`
	Patches      []string            `yaml:"patches"`
}

// Manifest represents the whole manifest file, contains the list of patches
// and the versions it affects
type Manifest struct {
	Patches  map[string]Patch   `yaml:"patches,omitempty"`
	Versions map[string]Version `yaml:"versions,omitempty"`
}

// New creates a new Manifest loading the configuration from the provided reader
func New(r io.Reader) (Manifest, error) {
	m := Manifest{}
	buf, err := ioutil.ReadAll(r)
	if err != nil {
		return m, fmt.Errorf("could not read configuration: %s", err)
	}

	err = yaml.Unmarshal(buf, &m)
	if err != nil {
		return m, fmt.Errorf("could not parse the configuration %s", err)
	}
	return m, nil
}

// Validate returns an error if the manifest is not correctly configured for the given version and environment
func (m Manifest) Validate(version string, environment string) error {
	for n, p := range m.Patches {
		if len(strings.TrimSpace(p.Checksum)) == 0 {
			return fmt.Errorf("patch %s does not contain an md5 checksum", n)
		}
		if len(strings.TrimSpace(p.BaseDir)) == 0 {
			return fmt.Errorf("patch %s does not contain a base dir", n)
		}
	}

	v, ok := m.Versions[version]
	if !ok {
		return fmt.Errorf("could not find version %s in manifest", version)
	}

	env, ok := v.Environments[environment]
	if !ok {
		return fmt.Errorf("could not find environment %s for version %s in manifest", environment, version)
	}

	if len(env) == 0 {
		return fmt.Errorf("there are no roles configured for environment %s on version %s", environment, version)
	}

	if len(v.Patches) == 0 {
		return fmt.Errorf("there are no patches configured for environment %s on version %s", environment, version)
	}

	for _, p := range v.Patches {
		if _, ok := m.Patches[p]; !ok {
			return fmt.Errorf("could not find patch %s referenced in version %s and environment %s", p, environment, version)
		}
	}

	return nil
}
